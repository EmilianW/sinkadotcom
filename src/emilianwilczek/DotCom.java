/**
 * 16013977 Emilian Wilczek
 * SinkADotCom
 * 05/10/2020
 */

package emilianwilczek;

import java.util.ArrayList;

/**
 * A single DotCom.
 */
public class DotCom
{
    private ArrayList<String> locationCells;
    private String name;

    /**
     * Updates DotComs loctions.
     * @param loc
     */
    public void setLocationCells(ArrayList<String> loc)
    {
        locationCells = loc;
    }

    /**
     * Name setter.
     * @param newName
     */
    public void setName(String newName)
    {
        name = newName;
    }

    /**
     * Determines hit or miss (or kill)
     * @param userInput
     * @return
     */
    public String checkYourself(String userInput)
    {
        String result = "miss";

        int index = locationCells.indexOf(userInput);

        if (index >= 0)
        {
            locationCells.remove(index);

            if (locationCells.isEmpty())
            {
                result = "kill";
                System.out.println("You sunk: " + name + "!");
            }
            else
            {
                result = "hit";
            }
        }
       return result;
    }
}
