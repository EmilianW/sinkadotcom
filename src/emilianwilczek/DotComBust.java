/**
 * 16013977 Emilian Wilczek
 * SinkADotCom
 * 05/10/2020
 */

package emilianwilczek;

import java.util.*;

/**
 * Main class of the game.
 */
public class DotComBust
{
    private GameHelper helper = new GameHelper();
    private ArrayList<DotCom> dotComsList = new ArrayList<DotCom>();
    private int numOfGuesses = 0;

    /**
     * Sets up the dot coms and gives instructions.
     */
    private void setUpGame()
    {
        DotCom one = new DotCom();
        one.setName("Pets.com");

        DotCom two = new DotCom();
        two.setName("eToys.com");

        DotCom three = new DotCom();
        three.setName("Go2.com");

        dotComsList.add(one);
        dotComsList.add(two);
        dotComsList.add(three);

        System.out.println("Your goal is to sink three dot coms.");
        System.out.println("Pets.com, eToys.com, Go2.com");
        System.out.println("Try to sink them all in the fewest number of guesses");

        for (DotCom dotComToSet : dotComsList)
        {
            ArrayList<String> newLocation = helper.placeDotCom(3);

            dotComToSet.setLocationCells(newLocation);
        }
    }

    /**
     * Basic gameplay/input loop.
     */
    private void startPlaying()
    {
        while(!dotComsList.isEmpty())
        {
            String userGuess = helper.getUserInput("Enter a guess");
            checkUserGuess(userGuess);
        }
        finishGame();
    }

    /**
     * Checks whether user guess is correct and applies correct outcome.
     * @param userGuess
     */
    private void checkUserGuess(String userGuess)
    {
        numOfGuesses++;

        String result = "miss";

        for (DotCom dotComToTest : dotComsList)
        {
            result = dotComToTest.checkYourself(userGuess);

            if (result.equals("hit"))
            {
                break;
            }
            if (result.equals("kill"))
            {
                dotComsList.remove(dotComToTest);
                break;
            }
        }

        System.out.println(result);
    }

    /**
     * Displays end results
     */
    private void finishGame()
    {
        System.out.println("All Dot Coms are dead! Your stock is now worthless.");
        if (numOfGuesses <= 18)
        {
            System.out.println("It only took you " + numOfGuesses + " guesses.");
            System.out.println(" You got out before your options sank.");
        }
        else
        {
            System.out.println("Took you long enough. " + numOfGuesses + " guesses.");
            System.out.println("Fish are dancing with your options");
        }
    }

    /**
     * Main class
     * @param args
     */
    public static void main(String[] args)
    {
        DotComBust game = new DotComBust();
        game.setUpGame();
        game.startPlaying();
    }
}
